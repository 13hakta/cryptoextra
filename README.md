# Encrypted package builder
Dummy extra Illustrates encoding/decoding of data and files in MODx transport for publishing in MODSTORE.PRO store.

Contains classes and helpers that:
* receives key from MODSTORE for en/decoding
* build a package
* en/decrypt objects and related data
* en/decrypts file tree of source files
* preloads encryptor class

This builder only shows conceptional idea and these files must be added to your project and corrected according to your needs.
Having only these files you can not build ready-to-use transport zip.

Learn comments in commits.
