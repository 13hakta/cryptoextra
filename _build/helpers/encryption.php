<?php
/**
 * @var xPDOTransport $transport
 */

if (!class_exists('EncryptedVehicle')) {
	$classIndex = 0;

	if (!class_exists('xPDOObjectVehicle'))
		$transport->xpdo->loadClass('transport.xPDOObjectVehicle', XPDO_CORE_PATH, true, true);

	// Store info about encoder in first
	$payload = include($transport->path . $transport->signature . '/' . $transport->vehicles[$classIndex]['filename']);
	$path = $transport->path . $transport->signature . '/' . $payload['class'] . '/' . $payload['signature'] . '/';
	$transport->xpdo->loadClass('EncryptedVehicle', $path, true, true);
}

return true;