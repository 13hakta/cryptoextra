<?php

class EncryptedVehicle extends xPDOObjectVehicle
{
	public $class = 'EncryptedVehicle';
	const version = '2.0.0';
	const cipher = 'AES-256-CBC';

	/**
	 * @param $transport xPDOTransport
	 * @param $object
	 * @param array $attributes
	 */
	public function put(&$transport, &$object, $attributes = array())
	{
		parent::put($transport, $object, $attributes);
		if (defined('PKG_ENCODE_KEY')) {
			$this->payload['object_encrypted'] = $this->encode($this->payload['object'], PKG_ENCODE_KEY);
			unset($this->payload['object']);
			if (isset($this->payload['related_objects'])) {
				$this->payload['related_objects_encrypted'] = $this->encode($this->payload['related_objects'], PKG_ENCODE_KEY);
				unset($this->payload['related_objects']);
			}
			if (isset($this->payload['related_object_attributes'])) {
				$this->payload['related_object_attr_encrypted'] = $this->encode($this->payload['related_object_attributes'], PKG_ENCODE_KEY);
				unset($this->payload['related_object_attributes']);
			}

			$this->payload[xPDOTransport::ABORT_INSTALL_ON_VEHICLE_FAIL] = true;

			$transport->xpdo->log(xPDO::LOG_LEVEL_INFO, 'Vehicle encrypted!');
		}
	}
	/**
	 * @param $transport xPDOTransport
	 *
	 * @return bool
	 */
	public function store(& $transport) {
		$stored = parent::store($transport);

		if (defined('PKG_ENCODE_KEY')) {
			$path = $transport->path . $transport->signature . '/' . $this->payload['class'] . '/' . $this->payload['signature'];

			foreach ($this->payload['resolve'] as $k => $v) {
				if ($v['type'] == 'file')
					if (!$this->encodeTree($path . '/' . $k)) return false;
			}
		}

		return $stored;
	}
	/**
	 * @param string $path
	 *
	 * @return bool
	 */
	protected function encodeTree($path) {
		$Directory = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
		$Iterator = new RecursiveIteratorIterator($Directory, RecursiveIteratorIterator::LEAVES_ONLY);

		foreach ($Iterator as $filename => $object) {
			$contents = file_get_contents($filename);
			$contents = $this->encode($contents, PKG_ENCODE_KEY);
			if (!file_put_contents($filename, $contents)) return false;
		}

		return true;
	}
	/**
	 * @param string $path
	 *
	 * @return bool
	 */
	protected function decodeTree($path) {
		$Directory = new RecursiveDirectoryIterator($path, FilesystemIterator::SKIP_DOTS);
		$Iterator = new RecursiveIteratorIterator($Directory, RecursiveIteratorIterator::LEAVES_ONLY);

		foreach ($Iterator as $filename => $object) {
			$contents = file_get_contents($filename);
			$contents = EncryptedVehicle::decode($contents);
			if (!file_put_contents($filename, $contents)) return false;
		}

		return true;
	}
	/**
	 * @param $transport xPDOTransport
	 * @param $options
	 *
	 * @return bool
	 */
	public function install(&$transport, $options)
	{
		if ($this->decodePayloads($transport, 'install')) {
			$transport->xpdo->log(xPDO::LOG_LEVEL_INFO, 'Vehicle decrypted!');
		} else {
			$transport->xpdo->log(xPDO::LOG_LEVEL_ERROR, 'Vehicle not decrypted!');
			return false;
		}
		return parent::install($transport, $options);
	}
	/**
	 * @param $transport xPDOTransport
	 * @param $options
	 *
	 * @return bool
	 */
	public function uninstall(&$transport, $options)
	{
		if ($this->decodePayloads($transport, 'uninstall')) {
			$transport->xpdo->log(xPDO::LOG_LEVEL_INFO, 'Vehicle decrypted!');
		} else {
			$transport->xpdo->log(xPDO::LOG_LEVEL_ERROR, 'Vehicle not decrypted!');
			return false;
		}
		return parent::uninstall($transport, $options);
	}
	/**
	 * @param array $data
	 *
	 * @return string
	 */
	protected function encode($data, $key)
	{
		$ivLen = openssl_cipher_iv_length(EncryptedVehicle::cipher);
		$iv = openssl_random_pseudo_bytes($ivLen);
		$cipher_raw = openssl_encrypt(serialize($data), EncryptedVehicle::cipher, $key, OPENSSL_RAW_DATA, $iv);
		return base64_encode($iv . $cipher_raw);
	}
	/**
	 * @param string $string
	 *
	 * @return string
	 */
	protected static function decode($string)
	{
		$ivLen = openssl_cipher_iv_length(EncryptedVehicle::cipher);
		$encoded = base64_decode($string);
		if (ini_get('mbstring.func_overload')) {
			$strLen = mb_strlen($encoded, '8bit');
			$iv = mb_substr($encoded, 0, $ivLen, '8bit');
			$cipher_raw = mb_substr($encoded, $ivLen, $strLen, '8bit');
		} else {
			$iv = substr($encoded, 0, $ivLen);
			$cipher_raw = substr($encoded, $ivLen);
		}
		return unserialize(openssl_decrypt($cipher_raw, EncryptedVehicle::cipher, PKG_ENCODE_KEY, OPENSSL_RAW_DATA, $iv));
	}
	/**
	 * @param $transport xPDOTransport
	 * @param string $action
	 *
	 * @return bool
	 */
	protected function decodePayloads(&$transport, $action = 'install')
	{
		if (isset($this->payload['object_encrypted']) || isset($this->payload['related_objects_encrypted'])) {
			if (!defined('PKG_ENCODE_KEY')) {
				if (!$key = $this->getDecodeKey($transport, $action)) {
					$transport->xpdo->log(xPDO::LOG_LEVEL_ERROR, "Decode key not received");
					return false;
				}
				define('PKG_ENCODE_KEY', $key);
			}

			if (isset($this->payload['object_encrypted'])) {
				$this->payload['object'] = $this->decode($this->payload['object_encrypted']);
				unset($this->payload['object_encrypted']);
			}
			if (isset($this->payload['related_objects_encrypted'])) {
				$this->payload['related_objects'] = $this->decode($this->payload['related_objects_encrypted']);
				unset($this->payload['related_objects_encrypted']);
			}
			if (isset($this->payload['related_object_attr_encrypted'])) {
				$this->payload['related_object_attributes'] = $this->decode($this->payload['related_object_attr_encrypted']);
				unset($this->payload['related_object_attr_encrypted']);
			}
		}
		return true;
	}
	/**
	 * @param $transport xPDOTransport
	 * @param $action
	 *
	 * @return bool|string
	 */
	protected function getDecodeKey(&$transport, $action)
	{
		$key = false;
		$endpoint = 'package/decode/' . $action;

		/** @var modTransportPackage $package */
		$package = $transport->xpdo->getObject('transport.modTransportPackage', array(
			'signature' => $transport->signature,
		));

		if ($package instanceof modTransportPackage) {
			/** @var modTransportProvider $provider */
			if ($provider = $package->getOne('Provider')) {
				$provider->xpdo->setOption('contentType', 'default');
				$params = array(
					'package' => $package->package_name,
					'version' => $transport->version,
					'username' => $provider->username,
					'api_key' => $provider->api_key,
					'vehicle_version' => self::version,
				);
				$response = $provider->request($endpoint, 'POST', $params);
				if ($response->isError()) {
					$msg = $response->getError();
					$transport->xpdo->log(xPDO::LOG_LEVEL_ERROR, $msg);
				} else {
					$data = $response->toXml();
					if (!empty($data->key)) {
						$key = $data->key;
					} elseif (!empty($data->message)) {
						$transport->xpdo->log(xPDO::LOG_LEVEL_ERROR, $data->message);
					}
				}
			} else
				$transport->xpdo->log(xPDO::LOG_LEVEL_ERROR, "Set MODStore as a provider in package details");

		}
		return $key;
	}
}
